{-# LANGUAGE TypeOperators, DataKinds, KindSignatures, TypeFamilies, UndecidableInstances #-}
{-# OPTIONS_GHC -cpp -pgmP./zuker #-}

module Cation.TypeSystem.Base where

import GHC.TypeLits

data a :~ b
data a :# b
data a :@ b
data a :++: b

data Q a
data C (a :: Symbol)
data T (a :: Symbol)
data U
data W
data P (a :: Nat)
data N (a :: Nat)
data O (a :: Ordering)
data PassTheSpent (a :: Symbol)
data Omitted a
data Disabled a
data Pattern a

data Fail (a :: Symbol) b

type family Eval (n :: Nat) a b where
  Eval 1 a a = Eval 0 a (Rewrite (Enable a))
  Eval n a a = a
  Eval n a b = Eval 1 b (Rewrite b)

type family Enable a where
  Enable (a :~ Disabled b) = Enable a :~ b
  Enable (a :~ b) = Enable a :~ b
  Enable (a :# b) = Enable a :# b
  Enable a = a

type family Bind a (b :: Symbol) c where
  Bind a b (c :1~ Q (C b)) = Bind a b c :1~ Q a
  Bind a b (c :1~ d)       = Bind a b c :1~ d
  Bind a b (c :~ d)        = Bind a b c :~ d
  Bind a b (c :# d)        = Bind a b c :# d
  Bind a b W               = W

type family Concat a b where
  Concat U a = a
  Concat (a :~ b) (W :~ c) = a :~ b :~ c
  Concat a (b :~ c) = Concat a b :~ c
  Concat (a :~ b) c = a :~ b :~ c

type family DropHide a where
  DropHide (a :# (W :~ b)) = a :# U
  DropHide (a :# (b :~ c)) = a :# b
  DropHide (a :# U) = DropHide a :# U
  DropHide (a :# b) = a :# U
  DropHide (a :~ b) = DropHide a :~ b
  DropHide a = Fail "DropHide" a

type family TakeHide a where
  TakeHide (a :# (W :~ b)) = b
  TakeHide (a :# (b :~ c)) = c
  TakeHide (a :# U) = TakeHide a
  TakeHide (a :# b) = b
  TakeHide (a :~ b) = TakeHide a
  TakeHide a = Fail "TakeHide" a

type family Fallback a b where
  Fallback a (Fail e b) = a
  Fallback a b = b

type family Failing (a :: Nat) b where
  Failing n ((Fail e a) :~ b) = Fail e (a :~ b)
  Failing n (a :~ (Fail e b)) = Fail e (a :~ b)
  Failing n ((Fail e a) :# b) = Fail e (a :# b)
  Failing n (a :# (Fail e b)) = Fail e (a :# b)
  Failing 1 (a :~ b) = Failing 0 (Failing 1 a :~ Failing 1 b)
  Failing n a = a

type family TryRewrite a where
  TryRewrite a = FailIfSame "TryRewrite" a (Rewrite a)

type family FailIfSame a b c where
  FailIfSame m a a = Fail m (Q (P 0))
  FailIfSame m a b = Succ m b

type RW a = Rewrite a  
type TR a = TryRewrite a
type FL a = Failing 1 a
type FAIL = Fail "Quick" (Q (P 0))

data Succ (a :: Symbol) b

data Ticket a
data Unticket a

type family Rewrite a where

  Rewrite ( ( a :1~ Q (C b) :2~ Q (C d) :3~ T "send" ) :@ ( e :4~ Q (C f) :5~ Q (C d) :6~ T "recv") ) = (a :123~ U) :@ (Bind (C b) f (e :456~ U))

  Rewrite ( ( a :1~ Q b :2~ Q (C d) :3~ T "send" ) :@ ( e :4~ Q (C d) :5~ T "recv") ) = (a :123~ U) :@ (e :45~ Q b)
  Rewrite ( ( e :4~ Q (C d) :5~ T "recv" :# h6) :@ ( a :1~ Q b :2~ Q (C d) :3~ T "send" ) ) = (a :123~ U) :@ (e :45~ Q b :6~ U)

  Rewrite ( (a :~ b :# d) :@ (c :~ T "send") ) = Unticket ( (a :# Ticket (b :# d)) :@ (c :~ T "send") )
  --Rewrite ( (a :~ T "add" :# U) :@ (c :~ T "send") ) = Unticket ( (a :# Ticket (T "add" :# U)) :@ (c :~ T "send") )

  Rewrite (a :1~ Q (P b) :2~ Q (P c) :3~ T "mul") = a :1~ Q (P (b*c)) :23~ U
  Rewrite (a :1~ Q (N b) :2~ Q (N c) :3~ T "mul") = a :1~ Q (P (b*c)) :23~ U
  Rewrite (a :1~ Q (N b) :2~ Q (P c) :3~ T "mul") = a :1~ Q (N (b*c)) :23~ U
  Rewrite (a :1~ Q (P b) :2~ Q (N c) :3~ T "mul") = a :1~ Q (N (b*c)) :23~ U

  Rewrite (a :1~ Q (P b) :2~ Q (P c) :3~ T "add") = a :1~ Q (P (b+c)) :23~ U
  Rewrite (a :1~ Q (N b) :2~ Q (N c) :3~ T "add") = a :1~ Q (N (b+c)) :23~ U
  Rewrite (a :1~ Q (N b) :2~ Q (P c) :3~ T "add") = a :1~ Q (N b) :2~ Q (P c) :3~ T "cmpNat" :U~ T "plug" :U~ T "plug" :U~ T "addWithCmpNat"
  Rewrite (a :1~ Q (P c) :2~ Q (N b) :3~ T "add") = a :1~ Q (N b) :2~ Q (P c) :3~ T "cmpNat" :U~ T "plug" :U~ T "plug" :U~ T "addWithCmpNat"

  Rewrite (a :1~ Q (N b) :2~ Q (P c) :3~ T "cmpNat") = a :1~ Q (O (CmpNat b c)) :# (W :~ Q (N b) :~ Q (P c)) :23~ U

  Rewrite (a :1~ Q (O 'GT) :2~ Q (N b) :3~ Q (P c) :4~ T "addWithCmpNat") = a :1~ Q (N (b-c)) :234~ U
  Rewrite (a :1~ Q (O 'LT) :2~ Q (N b) :3~ Q (P c) :4~ T "addWithCmpNat") = a :1~ Q (N (c-b)) :234~ U
  Rewrite (a :1~ Q (O 'EQ) :2~ Q (N b) :3~ Q (P c) :4~ T "addWithCmpNat") = a :1~ Q (P 0) :234~ U

  Rewrite (a :~ T "plug") = Fallback (a :~ Disabled (T "plug")) (Failing 1 (DropHide a :~ TakeHide a))
 
  Rewrite (a :# (W :~ U)) = a :# U
  Rewrite (a :# (b :~ U)) = a :# b

  Rewrite (a :# b :# c) = a :# (Concat b c)

  Rewrite (a :~ (b :# c)) = a :~ b :# c

  Rewrite (a :# b) = Rewrite a :# b
  Rewrite (a :~ b) = Rewrite a :~ b


  Rewrite (Pattern (Succ a b :@ c)) = b :@ c
  Rewrite ((Succ a b :@ c) :++: d) = Succ a b :@ c
  Rewrite ((Fail a b :@ c) :++: d) = d
  Rewrite (a :++: (Fail b c :@ d)) = a
  Rewrite (a :++: b :++: c) = Rewrite (a :++: b) :++: c

  Rewrite ((W :# a) :@ b) = b

  Rewrite (a :@  b :@ c)  = Pattern ( (TR (a :@ b) :@ c) :++: (TR (a :@ c) :@ b) :++: (TR (b :@ c) :@ a) :++: Succ "OtherwiseTryRewrite" (a :@  b :@ c ) )
  Rewrite (a :@ (b :@ c)) = Pattern ( (TR (a :@ b) :@ c) :++: (TR (a :@ c) :@ b) :++: (TR (b :@ c) :@ a) :++: Succ "OtherwiseTryRewrite" (a :@ (b :@ c)) )

  Rewrite (a :@ b) = RW a :@ RW b

  Rewrite (Pattern a) = Pattern (Rewrite a)

  Rewrite (Unticket (a :# Ticket b)) = a :U~ b
  Rewrite (Unticket a) = Unticket (Rewrite a)


  Rewrite W = W
  Rewrite (a :++: b) = (a :++: b)
  Rewrite (Succ a b) = Succ a (Rewrite b)
  Rewrite (Fail a b) = Fail a b
  Rewrite (Ticket a) = Ticket a
  --Rewrite a = a

test01 :: Eval 1 U (W :U~ Q (N 330) :U~ Q (P 220) :U~ T "mul")
test01 = undefined

test02 :: Eval 1 U (  (W :U~ Q (C "kukuruza") :U~ Q (C "tubino") :U~ T "send")
                   :@ (W :U~ Q (P 290) :U~ Q (C "kukuruza") :U~ T "send")
                   :@ (W :U~ Q (P 50) :U~ Q (C "zerno") :U~ T "recv" :U~ T "add" :U~ Q (C "zerno") :U~ Q (C "tubino") :U~ T "recv")
                   )
test02 = undefined










