{-# LANGUAGE TypeOperators, DataKinds, KindSignatures, TypeFamilies, UndecidableInstances #-}

module Cation.TypeSystem.SharNELL.Types where

data Yiel a
data Idle a
data MCUnit
data MDUnit
data ACUnit
data ADUnit
data Times a b
data Concat a b
data Atom a
data Invert a
data Alter a b
data Bottom a
data Discard a b
data Release a
data W

type T a = Atom a
type I a = Invert a
type B a = Bottom a
type a :~ b = Concat a b
type a :* b = Times a b
type a :% b = Alter a b
type a :& b = Discard a b
infixr :*
infixr :~
infixl :&

data a :++: b
infixl :++:












