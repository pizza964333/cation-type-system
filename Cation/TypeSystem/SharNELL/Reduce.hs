{-# LANGUAGE TypeOperators, DataKinds, KindSignatures, TypeFamilies, UndecidableInstances #-}

module Cation.TypeSystem.SharNELL.Reduce where
import Cation.TypeSystem.SharNELL.Types

-- Литература
-- A System of Interaction and Structure IV: The Exponentials and Decomposition https://arxiv.org/pdf/0903.5259.pdf System SNEL
-- The Undecidability of System NEL https://www.lix.polytechnique.fr/~lutz/papers/NELundeci.pdf

-- Правила редукции всегда связаны с необратимым действием, например сокращение атомов или изменения в структуре
-- так как написать правила как для инверсии так и для комутации решает больше проблем чем создаёт то тут именно такие дубли

-- RollDown направление редукции которое соответствует редукции pi исчисления
-- в pi исчислении сокращение каналов происходит именно в мультипликативной конюнкции (продукт процессов)
type family RollDown a where

  -- сокращение двух близких атомов
  RollDown ( T a :* I(T a) ) = MCUnit
  RollDown ( I(T a) :* T a ) = MCUnit

  -- сокращение стэка с левой стороны если в продукте есть инверсия (противоположенный атом)
  RollDown ( (   T a  :~ b ) :* I(T a) ) = b
  RollDown ( ( I(T a) :~ b ) :*   T a  ) = b
  RollDown (   T a  :* ( I(T a) :~ b ) ) = b
  RollDown ( I(T a) :* (   T a  :~ b ) ) = b

  -- сокращение стэка с правой стороны, это возможно благодаря наличию редукции в левосторонней нормальной форме
  RollDown ( ( a :~   T b  ) :* I(T b) ) = a
  RollDown ( ( a :~ I(T b) ) :*   T b  ) = a
  RollDown (   T a  :* ( b :~ I(T a) ) ) = b
  RollDown ( I(T a) :* ( b :~   T a  ) ) = b

  -- взаимное сокращение двух стэков с левой стороны
  RollDown ( (   T a  :~ b ) :* ( I(T a) :~ c ) ) = b :* c
  RollDown ( ( I(T a) :~ b ) :* (   T a  :~ c ) ) = b :* c

  -- взаимное сокращение двух стэков с правой стороны
  RollDown ( ( a :~   T b  ) :* ( c :~ I(T b) ) ) = a :* c
  RollDown ( ( a :~ I(T b) ) :* ( c :~   T b  ) ) = a :* c

  -- взаимное сокращение стэков в месте соединения, тоесть это конкатенация возникающая после сокрашения стыкующих атомов
  RollDown ( ( a :~   T b  ) :* ( I(T b) :~ c ) ) = a :~ c
  RollDown ( ( a :~ I(T b) ) :* (   T b  :~ c ) ) = a :~ c
  RollDown ( (   T a  :~ b ) :* ( c :~ I(T a) ) ) = c :~ b
  RollDown ( ( I(T a) :~ b ) :* ( c :~   T a  ) ) = c :~ b

  -- взаимное сокращение не крайних элементов из двух стэков, сдесь может быть такая форма что и правая и левая часть содержит ряд элементов
  RollDown ( ( a :~   T b  :~ c ) :* ( d :~ I(T b) :~ e ) ) = (a :* d) :~ (c :* e)
  RollDown ( ( a :~ I(T b) :~ c ) :* ( d :~   T b  :~ e ) ) = (a :* d) :~ (c :* e)

  -- крайний элемент сокращяет некрайний, элемент на левом краю стэка
  RollDown ( ( a :~   T b  :~ c ) :* ( I(T b) :~ d ) ) = a :~ ( c :* d )
  RollDown ( ( a :~ I(T b) :~ c ) :* (   T b  :~ d ) ) = a :~ ( c :* d )
  RollDown ( (   T a  :~ b ) :* ( c :~ I(T a) :~ d ) ) = c :~ ( b :* d )
  RollDown ( ( I(T a) :~ b ) :* ( c :~   T a  :~ d ) ) = c :~ ( b :* d )

  -- крайний элемент сокращяет некрайний, элемент на правом краю стэка
  RollDown ( ( a :~   T b  :~ c ) :* ( d :~ I(T b) ) ) = ( a :* d ) :~ c
  RollDown ( ( a :~ I(T b) :~ c ) :* ( d :~   T b  ) ) = ( a :* d ) :~ c
  RollDown ( ( a :~   T b  ) :* ( c :~ I(T b) :~ d ) ) = ( a :* c ) :~ d
  RollDown ( ( a :~ I(T b) ) :* ( c :~   T b  :~ d ) ) = ( a :* c ) :~ d

  -- сокращение элемента стэка в произвольном не крайнем месте
  RollDown ( ( a :~   T b  :~ c ) :* I(T b) ) = a :~ c
  RollDown ( ( a :~ I(T b) :~ c ) :*   T b  ) = a :~ c
  RollDown (   T a  :* ( b :~ I(T a) :~ c ) ) = b :~ c
  RollDown ( I(T a) :* ( b :~   T a  :~ c ) ) = b :~ c

  RollDown a = W

--SH hsfile=Cation/TypeSystem/SharNELL/Reduce.hs
--SH gen01=`mktemp -u --suffix=CODEGEN`
--SH gen02=`mktemp -u --suffix=CODEGEN`
--SH gen03=`mktemp -u --suffix=CODEGEN`
--SH grep "^  RollDown" < $hsfile | sed 's,\:\*,:%,g;s,I(,B(,g;s,MCUnit,MDUnit,g;s,RollDown,ClimbUp,g' > $gen02
--SH cat $hsfile | awk 'BEGIN { hide=0 } /\-\- BEGIN.*CLIMBUP /{ hide=1; print $0 } { if (hide==0) { print $0 } }' > $gen01
--SH cat $hsfile | awk 'BEGIN { hide=1 } /\-\- END.*CLIMBUP /{ hide=0 } { if (hide==0) { print $0 } }' > $gen03
--SH cat $gen01 $gen02 $gen03 > $hsfile

type family ClimbUp a where
-- BEGIN GENERATED CLIMBUP RULES
  ClimbUp ( T a :% B(T a) ) = MDUnit
  ClimbUp ( B(T a) :% T a ) = MDUnit
  ClimbUp ( (   T a  :~ b ) :% B(T a) ) = b
  ClimbUp ( ( B(T a) :~ b ) :%   T a  ) = b
  ClimbUp (   T a  :% ( B(T a) :~ b ) ) = b
  ClimbUp ( B(T a) :% (   T a  :~ b ) ) = b
  ClimbUp ( ( a :~   T b  ) :% B(T b) ) = a
  ClimbUp ( ( a :~ B(T b) ) :%   T b  ) = a
  ClimbUp (   T a  :% ( b :~ B(T a) ) ) = b
  ClimbUp ( B(T a) :% ( b :~   T a  ) ) = b
  ClimbUp ( (   T a  :~ b ) :% ( B(T a) :~ c ) ) = b :% c
  ClimbUp ( ( B(T a) :~ b ) :% (   T a  :~ c ) ) = b :% c
  ClimbUp ( ( a :~   T b  ) :% ( c :~ B(T b) ) ) = a :% c
  ClimbUp ( ( a :~ B(T b) ) :% ( c :~   T b  ) ) = a :% c
  ClimbUp ( ( a :~   T b  ) :% ( B(T b) :~ c ) ) = a :~ c
  ClimbUp ( ( a :~ B(T b) ) :% (   T b  :~ c ) ) = a :~ c
  ClimbUp ( (   T a  :~ b ) :% ( c :~ B(T a) ) ) = c :~ b
  ClimbUp ( ( B(T a) :~ b ) :% ( c :~   T a  ) ) = c :~ b
  ClimbUp ( ( a :~   T b  :~ c ) :% ( d :~ B(T b) :~ e ) ) = (a :% d) :~ (c :% e)
  ClimbUp ( ( a :~ B(T b) :~ c ) :% ( d :~   T b  :~ e ) ) = (a :% d) :~ (c :% e)
  ClimbUp ( ( a :~   T b  :~ c ) :% ( B(T b) :~ d ) ) = a :~ ( c :% d )
  ClimbUp ( ( a :~ B(T b) :~ c ) :% (   T b  :~ d ) ) = a :~ ( c :% d )
  ClimbUp ( (   T a  :~ b ) :% ( c :~ B(T a) :~ d ) ) = c :~ ( b :% d )
  ClimbUp ( ( B(T a) :~ b ) :% ( c :~   T a  :~ d ) ) = c :~ ( b :% d )
  ClimbUp ( ( a :~   T b  :~ c ) :% ( d :~ B(T b) ) ) = ( a :% d ) :~ c
  ClimbUp ( ( a :~ B(T b) :~ c ) :% ( d :~   T b  ) ) = ( a :% d ) :~ c
  ClimbUp ( ( a :~   T b  ) :% ( c :~ B(T b) :~ d ) ) = ( a :% c ) :~ d
  ClimbUp ( ( a :~ B(T b) ) :% ( c :~   T b  :~ d ) ) = ( a :% c ) :~ d
  ClimbUp ( ( a :~   T b  :~ c ) :% B(T b) ) = a :~ c
  ClimbUp ( ( a :~ B(T b) :~ c ) :%   T b  ) = a :~ c
  ClimbUp (   T a  :% ( b :~ B(T a) :~ c ) ) = b :~ c
  ClimbUp ( B(T a) :% ( b :~   T a  :~ c ) ) = b :~ c
  ClimbUp a = W
-- END GENERATED CLIMBUP RULES

{-
grep "^--SH" < Cation/TypeSystem/SharNELL/Reduce.hs | sed 's,^.....,,' | sh; exit; -}
