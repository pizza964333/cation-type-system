{-# LANGUAGE TypeOperators, DataKinds, KindSignatures, TypeFamilies, UndecidableInstances #-}

module Cation.TypeSystem.SharNELL where

import GHC.TypeLits

data Empty
data Atom (a :: Symbol)
data Invert a
data Times a b
data Bottom a
data Alter a b
data WhyNot a
data OfCourse
data MUnit
data Concat a b
data Favour a
data Yiel b
data Idle b

type a :* b = Times a b
infixr :*
infixr :~
--infixr :++:
type T a = Atom a
type I a = Invert a

type a :~ b = Concat a b
data F a = Favour a

type RW a = Rewrite a
type SR a = Search MUnit a

data a :++: b

type family Rewrite a where

  Rewrite ( T a :* I(T a) ) = MUnit
  Rewrite ( I(T a) :* T a ) = MUnit

  Rewrite ( T a :* I(T a) :* b ) = b
  Rewrite ( I(T a) :* T a :* b ) = b

  Rewrite ( I(T a) :* b :* c ) = ((I(T a) :* b) :* c) :++: ((I(T a) :* c) :* b)
  Rewrite (   T a  :* b :* c ) = ((  T a  :* b) :* c) :++: ((  T a  :* c) :* b)

  Rewrite ( (  T a  :~ b) :* I(T a) ) = b
  Rewrite ( (I(T a) :~ b) :*   T a  ) = b

  Rewrite ( (  T a  :~ b) :* I(T a) :* c ) = b :* c
  Rewrite ( (I(T a) :~ b) :*   T a  :* c ) = b :* c

  Rewrite ( (  T a  :~ b) :* c :* d ) = (((  T a  :~ b) :* c) :* d) :++: (((  T a  :~ b) :* d) :* c)
  Rewrite ( (I(T a) :~ b) :* c :* d ) = (((I(T a) :~ b) :* c) :* d) :++: (((I(T a) :~ b) :* d) :* c)

  Rewrite ( (a :~   T b ) :* I(T b) ) = a
  Rewrite ( (a :~ I(T b)) :*   T b  ) = a

  Rewrite ( (a :~   T b ) :* I(T b) :* c ) = a :* c
  Rewrite ( (a :~ I(T b)) :*   T b  :* c ) = a :* c

  Rewrite ( (a :~   T b)  :* c :* d ) = (((a :~   T b ) :* c) :* d) :++: (((a :~   T b ) :* d) :* c)
  Rewrite ( (a :~ I(T b)) :* c :* d ) = (((a :~ I(T b)) :* c) :* d) :++: (((a :~ I(T b)) :* d) :* c)

  Rewrite (a :* Idle d) = Idle (a :* d)
  Rewrite (Idle b :* c) = Idle (b :* c)

  Rewrite (Yiel (Idle a)) = Idle a


  Rewrite (Yiel a :* b) = Yiel (a :* b)
  Rewrite (a :* Yiel b) = Yiel (a :* b)

  -- Rewrite (Idle a :++: (Yiel ((((Idle b :++: Yiel c) :* e) :* f) :* g))) = (((Idle b :++: (Yiel c)) :* e) :* f) :* g

  Rewrite (Idle a :++: Yiel b) = Idle a :++: FlipYiel 1 (Yiel (RW b))
  Rewrite (Yiel a :++: Idle b) = FlipYiel 1 (Yiel (RW a)) :++: Idle b
  Rewrite (Yiel a :++: Yiel b) = FlipYiel 1 (Yiel (RW a)) :++: FlipYiel 1 (Yiel (RW b))
 
  Rewrite (Idle b :++: Idle d) = Idle b
  Rewrite (Idle b :++: c)      = FlipYiel 1 (IdleEmpty (Idle b) c)
  Rewrite (a :++: Idle c)      = FlipYiel 1 (IdleEmpty (Idle c) a)

  Rewrite (MUnit :* a) = a
  Rewrite (a :* MUnit) = a
  Rewrite (a :* b) = RW a :* RW b

  Rewrite (a :++: b) = TRW a :++: TRW b

  Rewrite (Yiel a) = (ReduceYiel (Yiel (Rewrite a)))

  Rewrite a = a

type family FlipYiel (n :: Nat) a where

{- 
  FlipYiel n (Idle a :++: (Yiel ((((Idle b :++: Yiel c) :* e) :* f) :* g))) = (((Idle b :++: FlipYiel 0 (Yiel c)) :* e) :* f) :* g
  FlipYiel n (Idle a :++: (Yiel (((Idle b :++: Yiel c) :* e) :* f))) = ((Idle b :++: FlipYiel 0 (Yiel c)) :* e) :* f
  FlipYiel n (Idle a :++: (Yiel ((Idle b :++: Yiel c) :* e))) = (Idle b :++: FlipYiel 0 (Yiel c)) :* e

  FlipYiel 1 (Yiel a) = FlipYiel 0 (Yiel (FlipYiel 1 a))
  FlipYiel n (Yiel a) = Yiel (FlipYiel 1 a)
  FlipYiel 1 (a :++: b) = FlipYiel 0 (FlipYiel 1 a :++: FlipYiel 1 b)
  FlipYiel n (a :++: b) =             FlipYiel 1 a :++: FlipYiel 1 b
  FlipYiel 1 (a :* b) = FlipYiel 0 (FlipYiel 1 a :* FlipYiel 1 b)
  FlipYiel n (a :* b) = FlipYiel 1 a :* FlipYiel 1 b
-}

  FlipYiel n a = a


type family IdleEmpty a b where
  IdleEmpty (Idle a) (Idle b :++: c) = Idle a :++: c
  IdleEmpty (Idle a) (b :++: Idle c) = Idle a :++: b
  IdleEmpty (Idle a) (b :++: c :++: d) = IdleEmpty (Idle a) (b :++: c) :++: b
  IdleEmpty (Idle a) b = Idle a :++: b

type TRW a = TryRW MUnit a

type family TryRW a b where
  TryRW a   (Idle c) = Idle c
  TryRW (Yiel a) (Yiel a) = Yiel a
  TryRW a     a = Idle a
  TryRW MUnit b = TryRW b (RW b)
  TryRW a     (Yiel b) = TryRW (Yiel b) (RW (Yiel b))
  TryRW a     b = TryRW (Yiel b) (RW (Yiel b))

type family ReduceYiel a where
   ReduceYiel (Yiel (Yiel c)) = ReduceYiel (Yiel c)
   ReduceYiel a = a

type family Search a b where
  Search a a = a
  Search a b = Search (RNF b) (RW (RNF (RW (LNF (RW b)))))


type family LNF a where
  LNF (a :~   (b :~   c)) = LNF (LNF (LNF a :~   LNF b) :~   LNF c)
  LNF (a :~ b) = LNF a :~ LNF b
  LNF (a :* b) = LNF a :* LNF b
  LNF (a :++: b) = LNF a :++: LNF b
  LNF a = a

type family RNF a where
  RNF ((a :~   b) :~   c) = RNF (RNF a :~   RNF (RNF b :~   RNF c))
  RNF (a :~ b) = RNF a :~ RNF b
  RNF (a :* b) = RNF a :* RNF b
  RNF (a :++: b) = RNF a :++: RNF b
  RNF a = a

test01 :: Rewrite ( Times (Atom "A") (Invert (Atom "A")) )
test01 = undefined

test02 :: SR ( I(T"A") :* T"B" :* T"A" )
test02 = undefined


test03 :: SR ( ( I(T"A") :~ T "C" )  :* T"B" :* T"A" )
test03 = undefined


test04 :: SR ( ( I(T"A") :~ T "C" ) :* T"A" )
test04 = undefined

test05 :: LNF ( T"A" :~ T"B" :~ T"C" :~ T"D" :~ T"E" :~ T"F" )
test05 = undefined


test06 :: SR ( ( T"C" :~ T"B" :~ T"D" :~ I(T"A") ) :* T"A" )
test06 = undefined

test07 :: RW (LNF ( ( T"C" :~ T"B" :~ T"D" :~ I(T"A") ) :* T"A" ))
test07 = undefined

test08 :: SR ( ( T"C" :~ T"B" :~ T"D" :~ I(T"A") ) :* T"E" :* T"F" :* T"G" :* T"H" :* T"A" )
test08 = undefined

--test09 :: SR ( ( T"C" :~ T"B" :~ T"D" :~ I(T"A") ) :* T"E" :* T"F" :* T"G" :* T"H" :* T"A" :* (T"A" :~ T"I" :~ T"J") :* (I(T"B") :~ I(T"I") :~ I(T"A") :~ I(T"J")) ) 
test09 :: SR (   ( T"C" :~ T"B" :~ T"D" :~ I(T"A") )
             :*    T"G" :* T"H" :* T"A"
             :* (I(T"B") :~ I(T"I") :~ I(T"A") :~ I(T"J"))
             :* (T"I" :~ T"J")
             ) 
test09 = undefined

