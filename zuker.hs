import Text.Parsec.String
import Text.Parsec
import System.Environment
import Data.List
--import Data.Symbol

main = do
  args <- getArgs
  let (out:"-o":inp:_) = reverse args
  Right a <- parseFromFile parser inp
  -- print a
  writeFile out (ss a)
  -- putStr (ss a)

fromRight (Right a) = a

ss (HideOp a b c) | elem 'U' (nub $ ss c) = ss a ++ ":# " ++ hidesToStr b ++ (delete 'U' $ ss c)
ss (HideOp a b c) = ss a ++ ":# " ++ hidesToStr b ++ " :~" ++ ss c
ss (Symbol a) = a
ss (Braces a) = "(" ++ concatMap ss a ++ ")"
ss (Text a) = a
ss (Eof) = ""
ss (Many a) = concatMap ss a
--ss (HashSymbol a) = let SW b = toSymbol a in "(SW " ++ show b ++ ")"
ss (Opt Nothing) = ""
ss (Opt (Just a)) = ss a

hidesToStr a = f a
 where
  f "U" = "U"
  f [a] = "h" ++ [a]
  f  b  = "(" ++ (foldl (\b a -> b ++ " :~ " ++ a) "W" $ map f $ map (:[]) b) ++ ")"

data Shugar = HideOp Shugar String Shugar | Symbol String | Braces [Shugar] | Text String | Eof | Many [Shugar] | HashSymbol String | Opt (Maybe Shugar)
 deriving (Eq,Ord,Read,Show)

parser :: Parser Shugar
parser = shugar

shugar = do
  a <- manyTill anyChar (lookAhead ((try hashSymbol <|> try hideOp) <|> (eof >> return Eof)))
  let b = do c <- hashSymbol <|> hideOp
             d <- shugar
             return (Many [Text a , c , d])
  (b <|> (eof >> return (Many (Text a:[Eof]))))

pattern :: Parser Shugar
pattern = hashSymbol <|> symbol <|> braces

sps = many space

braces = do
  a <- char '('
  sp1 <- sps
  let d = do b <- pattern
             sp2 <- sps
             return (Many [b,Text sp2])
  e <- many1 d
  c <- char ')'
  return (Braces (Text sp1 : e))

hashSymbol = do
  sp1 <- sps
  char '{'
  a <- letter
  b <- many alphaNum
  char '}'
  sp2 <- sps
  return $ Many [Text sp1, HashSymbol (a:b), Text sp2]

symbol = do
  sp1 <- sps
  a <- letter
  b <- many alphaNum
  sp2 <- sps
  return $ Many [Text sp1, Symbol (a:b), Text sp2]

hideOp = do
  p1 <- optionMaybe pattern
  sp1 <- sps
  a <- char ':'
  b <- many1 alphaNum <|> string "U"
  c <- char '~'
  sp2 <- sps
  p2 <- try hideOp <|> pattern
  return $ HideOp (Many [Opt p1,Text sp1]) b (Many [Text sp2, p2])

